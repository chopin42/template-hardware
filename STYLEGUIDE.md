# Style Guide
To make sure you are in prose with the rest of the project, please make sure your follow this guide.

## Principle 1
Explanation of your principle.

- :smile: Your good example
- :cry: Your bad example

## Principle 2
Explanation of your principle.

- :smile: Your good example
- :cry: Your bad example

## Principle 3
Explanation of your principle.

- :smile: Your good example
- :cry: Your bad example

## More guidance
If you have more questions you can open an [issue](https://gitea.com/USER/PROJECT/issues).
