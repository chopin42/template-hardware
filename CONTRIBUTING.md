# Contributing to PROJECT

:+1::tada: First off, thanks for taking the time to contribute! :tada::+1:

We've put together the following guidelines to help you figure out where you can best be helpful.

Use your best judgment, and feel free to propose changes to this document in a pull request.

## Table of Contents

0. [Types of contributions we're looking for](#types-of-contributions-were-looking-for)
0. [Ground rules & expectations](#ground-rules--expectations)
0. [How to contribute](#how-to-contribute)
0. [Style guide](#style-guide)
0. [Setting up your environment](#setting-up-your-environment)
0. [Contribution review process](#contribution-review-process)
0. [Community](#community)

## Types of contributions we're looking for
There are many ways you can directly contribute (in descending order of need):

* Needs 1
* Needs 2
* Needs 3
* Needs 4

Interested in making a contribution? Read on!

## Ground rules & expectations

Before we get started, here are a few things we expect from you (and that you should expect from others):

* Open Source Guides are released with a [Contributor Code of Conduct](./CODE-OF-CONDUCT.md). By participating in this project, you agree to abide by its terms.
* Rule 1
* Rule 2


## How to contribute

If you'd like to contribute, start by searching through the [issues](https://gitea.com/USER/PROJECT/issues) and [pull requests](https://gitea.com/USER/PROJECT/pulls) to see whether someone else has raised a similar idea or question.

If you don't see your idea listed, and you think it fits into the goals of this guide, do one of the following:
* **If your contribution is minor,** such as a EXAMPLE, open a pull request.
* **If your contribution is major,** such as a EXAMPLE, start by opening an issue first. That way, other people can weigh in on the discussion before you do any work.

## Style guide
If you're creating, see the [style guide](./STYLEGUIDE.md) to help your prose match the rest of the project.

## Setting up your environment

Explain how your project is made and what are the requirements.

## Community

Discussions about the project take place on this repository's [Issues](https://gitea.com/USER/PROJECT/issues) and [Pull Requests](https://gitea.com/USER/PROJECT/pulls) sections. Anybody is welcome to join these conversations. There is also a [mailing list](EMAILLISTURL) for regular updates.

Wherever possible, do not take these conversations to private channels, including contacting the maintainers directly. Keeping communication public means everybody can benefit and learn from the conversation. But if you got a **major security issue** maybe try to send your issue in private to developers via : YOUREMAILHERE
