# Your Project Title
A nice fresh description of your project over there.

![Screenshot of the result here](finished.png)

Extended description here.

## Get in touch
You don't want to build this by yourself? Not a problem you can buy one there:

[![icon to the shop](shop.png)](link to shop)

## Build from source!
You are a maker? You want to build it by yourself? Here you are!

Go on the [BUILD.md](./BUILD.md) file to build your own!

## Troubleshooting
### iSSUE ONE

Reolution of the issue

### ISSUE TWO

Resolution of the issue

### Not one of these

If your issue is not one of these search into the [issues](https://gitea/USER/PROJECT/issues) to find yours. If not exsist create one and wait for answer by the community and devloppers. If this issue is caused by a bug it will be solved in the next release. Thanks for contributing to this project!

## Contribute!

You want to contribute? Thanks! You can get all the information at [CONTRIBUTING guide](./CONTRIBUTING.md)

## Meta
This project has been created by YOURNAME, thanks to all contributors to make this possible! 

This project is under GNU GENERAL PUBLIC LICENSE, to get more information go to [LICENSE file](./LICENSE)